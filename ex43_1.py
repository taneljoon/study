# stuff I've learned

# if you copy in code check with Show All Characters
# you need to check the code for inconsitensies
# I kinda suck still, took so long to get everything right
# more emphasis needs to be put on spelling

from sys import exit
from random import randint

class Scene(object):

	def enter(self):
		print "Not defined yet"
		exit(1)

class Engine(object):

	def __init__(self, scene_map):
		self.scene_map = scene_map
		print "scene map initialized"
		
	def play(self):
		current_scene = self.scene_map.opening_scene()
		last_scene = self.scene_map.next_scene('finished')
		
		print "current scene is %s" % current_scene
		print "last scene is %s" % last_scene
		
		while current_scene != last_scene:
			print "current scene = last scene"
			next_scene_name = current_scene.enter()
			current_scene = self.scene_map.next_scene(next_scene_name)
			
		print current_scene
		print '\a'
		# be sure to print out the last scene
		current_scene.enter()

		
		
class Death(Scene):

	quips = [
		"You died.  You kinda suck at this.",
		"Your mom would be proud...if she were smarter.",
		"Such a luser.",
		"I have a small puppy that's better at this."]

	def enter(self):
		print ""
		from time import sleep
		sleep(2) # sleep
		print randint(0,len(self.quips)-1)
		print Death.quips[randint(0,len(self.quips)-1)]
		exit(1)

class CentralCorridor(Scene):

	def enter(self):
		print "class CentralCorridor activated"
		print "You have entered the escape pod"
		print "What would you like to do?"
		print "1. Nothing"
		print "2. Something"
		print "3. Anthing"
		
		choice = raw_input(">")
		
		if int(choice) == 1:
			# goto LaserWeaponArmory
			return "laser_weapon_armory"
		elif int(choice) == 2:
			# goto TheBridge
			return "the_bridge"
		elif int(choice) == 3:
			# goto EscapePod
			return "escape_pod"	
		else:
			# goto Death
			return "death"
							
class LaserWeaponArmory(Scene):
	
	def enter(self):
		print "Your were in the Armory"
		print "Go back to the central corridor"
		
		return "central_corridor"

class TheBridge(Scene):

	def enter(self):
		print "You were on the bride"
		print "go back to the central corridor"
		
		return "central_corridor"
		
class EscapePod(Scene):

	def enter(self):
		print "You win! YAY!"
		
		return "finished"

class Finished(Scene):

	def enter(self):
		print "VICTORY!"
		exit(1)

class Map(object):
	
	maplist = {
	'central_corridor': CentralCorridor(),
	'laser_weapon_armory': LaserWeaponArmory(),
	'the_bridge': TheBridge(),
	'escape_pod': EscapePod(),
	'death': Death(),
	'finished': Finished(),
	}
	
	def __init__(self, start_scene):
		self.start_scene = start_scene
		#print "class Map initilized"

	def next_scene(self, scene_name):
		val = Map.maplist.get(scene_name)
		return val
		#print "class Map next_scene initialized"
		
	def opening_scene(self):
		return self.next_scene(self.start_scene)
		#print "class Map opening_scene initialized"

		
a_map = Map('central_corridor') # activates Map
a_game = Engine(a_map) 

print "a_game is %s" % a_game
a_game.play() 

# from a_game get the play() function and call it
# with parameters self and whatever is in the brackets


"""
import test_module as aaa
aaa.print_something(1)
raw_input("Press ENTER to continue")
"""