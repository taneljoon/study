# -*- coding: utf-8 -*-
import hashmap

# cities
cities = hashmap.new()

hashmap.set(cities, 'Eesti', 'Tallinn')
hashmap.set(cities, 'Lätvi', 'Riga')
hashmap.set(cities, 'Leedu', 'Vilnius')
hashmap.set(cities, 'Soome', 'Helsinki')
hashmap.set(cities, 'Rootsi', 'Stockholm')

# districts
districts = hashmap.new()

# one per thing, it overwrites this stuff
hashmap.set(districts, 'Tallinn', 'Kesklinn')
hashmap.set(districts, 'Tallinn', 'Nõmme')
hashmap.set(districts, 'Tallinn', 'Lasnamäe')
hashmap.set(districts, 'Riga', 'Pommiauk')
hashmap.set(districts, 'Riga', 'Suurem pommiauk')
hashmap.set(districts, 'Vilnius', 'Mudaauk')
hashmap.set(districts, 'Vilnius', 'Poriauk')
hashmap.set(districts, 'Helsinki', 'Jukka linn')
hashmap.set(districts, 'Helsinki', 'Pekka linn')
hashmap.set(districts, 'Stockholm', 'Rikkurite linn')
hashmap.set(districts, 'Stockholm', 'Veel rikkamate linn')

# print out some cities
print '_' * 10
print "Eesti has: %s" % hashmap.get(cities, 'Eesti')
print "Leedu has: %s" % hashmap.get(cities, 'Leedu')

# do it by using the state then cities dict
print '-' * 10
print "Rootsi has: %s" % hashmap.get(districts, hashmap.get(cities, 'Rootsi'))

# print every state abbreviation
print '_' * 10
hashmap.list(cities)

# print every city in state
print '_' * 10
hashmap.list(districts)
