# __name__ is __main__
# it is the first module's name variable


if __name__ == "__main__":
    main()
	
# python sets up special variable, __name__ is set as equal as __main__
# if we import a module then it will be the name of the module
# ie 
# import first_module

# print __name__