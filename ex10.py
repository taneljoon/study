# example 10
# from http://learnpythonthehardway.org/book/ex10.html

# all about the \ characters and \n - new line stuff


tabby_cat = "\tI'm tabbed in."
persian_cat = "I'm split\non a line."
backslash_cat = "I'm \\ a \\ cat."

fat_cat = """
I'll do a list:
\t* Cat food
\t* Fishies
\t* Catnip\n\t* Grass
"""

print tabby_cat
print persian_cat
print backslash_cat
print fat_cat

# some tests

print "\\" # backslash
print "\'" # single quote
print "\"" # double quote
print "\a" # ASCII bell, bell sound 
print "\b\b\b\b" # ASCII backspace
print "\f" # ASCII formfeed
print "Oink \n Oink" # ASCII linefeed
print "\N{name}"
print "\N{03DA}" # Greek letter STIGMA

print "\t\rOINK" # Carriage Return
print "\tOINK" # horizontal tab

print "\uxxxx" # character with 16-bit hex value xxxx
print "\Uxxxxxxxx" # 32-bit hex

print "\u1010" # character with 16-bit hex value xxxx
print "\U10101000 \v OINK" # 32-bit hex

print "\v"	# ASCII vertical tab
print "\000" # character with octal value ooo
print "\101" # character with octal value ooo

# print "\xhh"
print "\x20" # character with the hex value hh


# unicode is an issue
