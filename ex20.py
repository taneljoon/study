# ex20

# import arguments
from sys import argv

# name arguments
script, input_file = argv

# define function that reads the input file
def print_all(f):
	print f.read()

# define function that seeks 0th byte, or rewinds the file
def rewind(f):
	f.seek(0)

# define function that prints a specified line from file
def print_a_line(line_count, f):
	print line_count, f.readline()

# define current file, open it
current_file = open(input_file)

print "First let's print the whole file:\n"

# use function print_all with current file as argument
print_all(current_file)

print "Now let's rewind, kind of like a tape."

# use rewind function
rewind(current_file)

print "Let's print three lines:"

# define current line, and then use function print a line
current_line = 1
print_a_line(current_line, current_file)

current_line += 1
#print current_line
print_a_line(current_line, current_file)

current_line += 1
#current_line = current_line + 1
print_a_line(current_line, current_file)

# current line is passed on to the function
# useful powershell pydoc file.seek