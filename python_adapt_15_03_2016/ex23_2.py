# ex23_2
# comment: works, copies the files
# to learn ...


# Script Name		: move_files_over_x_days.py
# Author				: Craig Richards
# Created				: 8th December 2011
# Last Modified		: 
# Version				: 1.0

# Modifications		: 

# Description			: This will move all the files from the src directory that are over 240 days old to the destination directory.

import shutil, sys, time, os								# Import the header files
src = 'H:\python_adapt'												# Set the source directory
dst = "test" #'H:\python_adapt\test'												# Set the destination directory

# if destinaiton folder doesn't exist then create it
if not(os.path.exists(dst)):
	os.mkdir(dst)

now = time.time()											# Get the current time
for f in os.listdir(src):										# Loop through all the files in the source directory
    if os.stat(f).st_mtime < now - 2 * 86400:	# Work out how old they are, if they are older than 2 days old
        if os.path.isfile(f):									# Check it's a file
            shutil.move(f, dst)								# Move the files 
			
