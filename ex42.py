# ex42'

# Animal is-a object
class Animal(object):
	pass

## dog-Animal - is-a
class Dog(Animal):
	def __init__(self, name):
		# def has-a
		self.name = name

# cat-animal - is-a		
class Cat(Animal):
	# funciton has-a
	def __init__(self, name):
		self.name = name

class Person(Object):
	# def has-a
	def __init__(self, name):
		# is-a
		self.name = name
		
		## Person has-a pet of some kind
		self.pet = None

# class is-a
class Employee(Person):
	def __init__(self, name, salary):
		## is-a
		super(Employee, self).__init__(name)
		# has-abs
		self.salary = salary
		
# is-a
class Fish(object):
	pass

# is-a
class Halibut(Fish):
	pass

# rover is-a dog
rover = Dog("Rover")

# is
mary = Person("Mary")

# has
mary.pet = rover

# is
flipper = Fish()	

# is
crouse = Salmon()

# is
harry = Halibut()

#print Dog.name