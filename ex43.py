# ex43 more OOP
# design process - 
# 1. Write or draw about the problem
# 2. Extract key concepts from 1 and research them
# 3. Create a class hierarchy and object map for the concepts
# 4. Code the classes and a test to run them
# 5. Repeat and refine

# this exercise is to show the basis of setting up classes

class Scene(object):

	def enter(self):
		pass

class Engine(object):

	def __init__(self, scene_map):
		pass
		
	def play(self):
		pass
		
class Death(Scene):
	
	def enter(self):
		pass
		
class CentralCorridor(Scene):
	
	def enter(self):
		pass

class LaserWeaponAromory(Scene):

	def enter(self):
		pass
		
class Map(object):

	def __init__(self, start_scene):
		pass
		
	def next_scene(self, scene_name):
		pass
		
	def opening_scene(self):
		pass
		
a_map = Map('central_corridor')
a_game = Engine(a_map)
a_game.play()