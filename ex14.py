# exercise 14

from sys import argv # argv is a regular way to express aguments

script, user_name, nickname = argv

prompt = '> '

print "Hi %s, I'm the %s script." % (user_name, script)
print "I'd like to ask you a few questions."
print "Do you like me %s?" % user_name

likes = raw_input(prompt)

print "Where do you live %s?" % user_name
lives = raw_input(prompt)

print "What kind of computer do you have?"
computer = raw_input(prompt)

print """
Alright, so you said %s about liking me.
You live in %s. Not sure where that is.
And you have a %s computer. Nice.
And your nickname is %s.
""" % (likes, lives, computer, nickname)

# %r for testing and %s for presentation
