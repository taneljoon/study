# ex38 doing things to lists

ten_things = "Apples Oranges Crows Telephone Light Sugar"
print len(ten_things)
print "Wait there not 10 things in that list. Let's fix that."

stuff = ten_things.split(' ')
more_stuff = ["Day", "Night", "Song", "Fisbee", "Corn", "Banana", "Girl", "Boy"]

while len(stuff) != 10:
	next_one = more_stuff.pop() # removes the item from the list and returns it
	print "Adding: ", next_one
	stuff.append(next_one)
	print "There are %d items now." % len(stuff)
	
print "There we go: ", stuff

print "Let's do some things with stuff."

print stuff[1]
print stuff[-1] # whoa! fancy
print stuff.pop()
print ' '.join(stuff) # what? cool!
print '#'.join(stuff[3:5]) # super stellar!

temp1 = '*'.join(stuff)
print temp1

#print pop(more_stuff)
#print pop(more_stuff)
