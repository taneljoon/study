# exercise 9

days = "Mon Tue Wed Thu Fri Sat Sun"
months = "Jan\nFeb\nMar\nApr\nMay\nJun\nJul\nAug"

print "Here are the days: ", days
print "Here are the months: ", months

print """
There's something going on here.
With the three double-quotes.
We'll be able to type as much as we like.
Even 4 lines if we want, or 5, or 6.
"""


# Made mistakes
# days, not day plural vs singular
# spaces in front
# also new line mistake in a repetative series

print "%r" % months
print "%s" % months
