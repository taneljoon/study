# doas_rooms
import random
from doas_encounter import * 
from doas_char_info import *


def outside(char):
	"""
	First location.
	"""
	
	print "You can enter the mansion."
	print "Or wou can wait."
	print "Or check your info"
	print ""
	
	char.status = "outside"
	
	choice = raw_input("> ")
	
	if choice == "enter":
		main_entrance(char)
	else:
		regulal_choices(char, choice)
		#print "Please enter input again."
		outside(char)

def check_conditions(char):
	"""
	checks if victory conditions are met
	"""
	
	if char.credit >= 120:
		print "you have enough credits to win the level"
		quit()
	

	

def regulal_choices(char, choice):
	""" 
	This is the module for regular choices.
	"""
	if choice == "wait":
		wait(char)
	elif choice == "info":
		char_info(char)
	elif choice == "quit":
		quit()
	else:
		print "Please insert input again."
	# go back to the module that was active before
	eval(char.status +"(char)")

def main_entrance(char):
	"""
	Move into this after outside.
	"""
	print "You inspect your surroundings."
	print "You can go left, forward or left."
	print "Please choose your next action."
	
	char.status = "main_entrance"
	
	check_conditions(char) # checks victory conditions
	choice = raw_input("> ")
	
	if choice == "left":
		print "go left"
		kitchen(char)
	elif choice == "right":
		print "go right"
		wardrobe(char)
	elif choice == "forward":
		print "go forward"
		dojo(char)
	else:
		regulal_choices(char, choice)
		
		#main_entrance(char)

def wait(char):
	"""
	Wait, restore stats, have a chance encounter.
	"""
	
	print "You wait"
	
	roll_encounter = random.random() # chance for encounter
	roll_encounter_level = int(10 * random.random()) # chance for level
	#print roll_mnst_level # for checking
	
	if roll_encounter > 0.9: 
		encounter(roll_encounter_level, char)
	
	
def kitchen(char):
	"""
	Move left from main_entrance.
	"""
	
	print "You are in the kitchen."
	
	roll_encounter = random.random()
	roll_encounter_level = int(10 * random.random())
	
	char.status = "kitchen"
	
	if roll_encounter and char.status != "kitchen" > 0.4:
		encounter(roll_encounter_level, char)
	
	print "Go back to main entrance"
	
	choice = raw_input(">")
	
	if choice == "back":
		main_entrance(char)
	else:
		regulal_choices(char, choice)	

def wardrobe(char):
	"""
	You are in the wardrobe.
	"""
	
	char.status = "wardrobe"
	print """
	You can search for cash.
	Or go back
	"""
	
	choice = raw_input("> ")
	
	if choice == "back":
		main_entrance(char)
	elif choice == "search":
		if char.mood == "bored":
			print "You can't search when you are bored."
			wardrobe(char)
		else:
			increase = int(10 * random.random())
			print "You find %d credits." % increase
			char.credit = char.credit + increase
			char.mood = "bored"
			wardrobe(char)
	else:
		regulal_choices(char, choice)

def dojo(char):
	"""
	You enter the dojo.
	"""
	
	if char.status == "dojo":
		encounter_override = 0
	else:
		encounter_override = 1
			
	if encounter_override == 1:
		roll_encounter_level = int(10 * random.random())
		encounter(roll_encounter_level,char)
	
	char.status = "dojo"
	
	print """
	You should go back to a safe area.
	"""
	
	choice = raw_input("> ")
	
	if choice == "back":
		main_entrance(char)
	
	else:
		regulal_choices(char, choice)
	
	