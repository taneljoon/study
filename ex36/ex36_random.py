# ex36_random

"""
# select randomly from list
list = [1,2,3,4,5]
print random.choice(list)

# mathematical functions
from math import sqrt
sqrt(20)


# open and write into files
f2 = open("test.out",'w')
f2.write("test")
f2.close

""" 


# string to variable name, I want to call function test(aa) 
# I have "test" in status,


name = "varname"
value = "something"

class self:
	pass
	
#import os
setattr(self, name, value)

print self.varname


def test(aa):
	print aa
	
cc = "early test"	
test(cc)

class input:
	pass

input.something_test = "test"

bb = "right answer"

# use eval for the more tedious but same action like
# info
# quit
# wait
# and more
eval(input.something_test+"(bb)")

"""
exec(input.something_test)
# exec can work as well 
# like 
program = 'for i in range(3):\n    print("Python is cool")'
exec(program)

#setattr()

#self.varname
usable_out = "wrong answer"
setattr(input, input.something_test, usable_out)
print usable_out
"""