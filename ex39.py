# ex 39 dictionary - dicts aka hashes

# getting started
stuff = {'name': 'Zed', 'age': 39, 'height': 6 * 12 +2}
print stuff['name']

print stuff['age']
print stuff['height']

stuff['city'] = "San Francisco"
print stuff['city']

# adding things into hashes
stuff[1] = "Wow"
stuff[2] = "Neato"

print stuff

del stuff['city']
del stuff[1]
del stuff[2]

print stuff

# TBC

# create a mapping of state to abbreviation
states = {
	'Oregon' : 'OR',
	'Florida' : 'FL',
	'California' : 'CA',
	'New York' : 'NY',
	'Michigan' : 'MI'
	}
	
# create a basic set of states and some cities in them 

cities = {
	'CA' : 'San Francisco',
	'MI' : 'Detroit',
	'FL' : 'Jacksonville'
	}
	
	
# add some more cities
cities['NY'] = 'New York'
cities['OR'] = 'Portland'

# print out some cities
print '_' * 10
print "NY State has: ", cities['NY']
print "OR State has:", cities['OR']

# print some states
print '_' * 10
print "Michingan's abbreviation is: ", states['Michigan']
print "Again but with \" marks", states["Michigan"]
print "Florida's abbreviaiton is: ", states['Florida']

# do it by using the state the cities dict
print '_' * 10
print "Michigan has: ", cities[states['Michigan']]
print "Florida has: ", cities[states['Florida']]

# print every state abbreviaiton
print '_' * 10
for state, abbrev in states.items():
	print "%s is abbreviated %s" % (state,abbrev)
	
# print every city in state
print '_' * 10
for abbrev, city in cities.items():
	print "%s has the city %s" % (abbrev, city)
	
# now do both at the same time
print '_' * 10
for state, abbrev in states.items():
	print "%s state in abbreviated %s and has city %s" % (
	state, abbrev, cities[abbrev])
	
print '_' *10
# safely get a abbreviation by state that might not be there
state = states.get('Texas')


if not state:
	print "Sorry, no Texas."
	
# get a city with a deafult values
city = cities.get('TX', 'Does Not Exist')
print "The city for the state 'TX' is %s" % city