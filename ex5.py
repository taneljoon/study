# -*- coding: utf-8 -*-

# this top line is used if non-ASCII characters are used

# exercise 5 
name = 'Zed A. Shaw'
age = 35 # heheh
height = 74 # inches
height = height * 2.54
weight = 180
weight = weight * 0.48
eyes = 'Blue'
teeth = 'White'
hair = 'Brown'

print "Let's talk about %s." % name
print "He's %d inches tall." % height
print "He's %d pounds heavy." % weight
print "Actually that's not too heavy."
print "He's got %s eyes and %s hair." % (eyes, hair)
print "His teeth are usally %s depending on the coffee." % teeth

# this line is tricky
print "If I add %d, %d, and %d I get %d." % (age,height,weight,age + height + weight)

print "Height %r" % height
print "Weight %r" % weight

print "Eyes %r" % eyes