# ex33_1 - convert to function



def loop_stuff(input, increment):
	i = 0
	numbers = []
	
	while i < input:
		print "At the top is %d" % i
		numbers.append(i)
		
		i = i + increment
		print "Numbers now: ", numbers
		print "At the bottom i is %d" % i
	return numbers

numbers_stuff = loop_stuff(18, 2)		

print numbers_stuff
print "The numbers: "

for num in numbers_stuff:
	print num