# ex40 - objects

class Song(object):
	def __init__(self, lyrics):
		self.lyrics = lyrics
		
	def sing_me_a_song(self):
		for line in self.lyrics:
			print line
			

happy_bday = Song(["Happy birthday to you",
					"I don't want to get sued",
					"So I'll stop right there"])
					
bulls_on_parade = Song(["They rally around tha family",
						"With pockets full of shells"])

happy_bday.sing_me_a_song()

bulls_on_parade.sing_me_a_song()

# write more lyrics

laika = Song(["Laika, suure Pauka poeg",
			"Laika, ema oli koer",
			"Ode ja ka vend, koeraks hyydsid end",
			"Laika, lambakoera poeg"])

laika.sing_me_a_song()

laika2 = ["Pauka, valvas karjamaid",
		"Vennad, konte koju toid",
		"Laika lulli loi, prygikastist soi",
		"Naabri Barbi kuuti toi"]

#print laika2[1]		
#Sing_me_a_song(laika2)

song2 = Song(laika2)

song2.sing_me_a_song()