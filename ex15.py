# example 15 - reading files

# import argument, sys is a package 
from sys import argv

# 2 arguments - the script name and filename
script, filename = argv

# opens a file and names the file object is named txt
txt = open(filename)

# prints filename
print "Here's your file %r:" % filename

# reads and then prints the file object
print txt.read()

# asks for the filename again
print "Type the filename again:"
file_again = raw_input("> ")

# file is opend and object named again
txt_again = open(file_again)

# file is read and printed
print txt_again.read()

txt.close()

txt_again.close()