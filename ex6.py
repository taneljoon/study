# exercise 6 
# x equals a string and a something value in the string
x = "There are %d types of people." % 10
binary = "binary"
do_not = "don't"
# y equals a string and two values in it
y = "Those who know %s and those who %s." % (binary, do_not)


# print x and y
print x
print y

print "I said: %r." % x
print "I also said: '%s' ." % y

hilarious = False
joke_evaluation = "Isn't that joke so funny?! %r" 

print joke_evaluation % hilarious

w = "This is the left side of..."
e = "a string with a right side."

# this prints the two strings w and e
print w + e


