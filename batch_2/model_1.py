# example calculation for iteration script

def model_main(el_prod, el_price, cost_fixed, cost_marg):
	"""Calculate the various outputs from electricity production model. 
	Returns operating profit.
	El_prod - in MWh
	El_price - EUR/MWh
	cost_fixed - in EUR
	cost_marg - EUR/MWh
	
	Return operating profit, ratio = el_price/cost_marg, gain_per_MWh
	"""
	
	cost_fixed = cost_fixed * (1 + (el_prod/50000) ** .2) # make some function to have non-linearity
	
	cost_total = el_prod * cost_marg + cost_fixed
		
	revenue_total = el_prod * el_price
	operating_profit = revenue_total - cost_total
	ratio = el_price / cost_marg
	gain_per_MWh = el_price - cost_marg
	return operating_profit, ratio, gain_per_MWh