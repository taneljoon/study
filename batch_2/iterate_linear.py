# iteration script

from sys import argv
from model_1 import model_main
from random import random


script, el_prod_temp = argv
el_prod = float(el_prod_temp)

#el_prod = 10000
el_price = 25.0
cost_fixed = 1000000
cost_marg = 18.0

print el_prod

inputs_vector = [el_prod, el_price, cost_fixed, cost_marg]

#profit = model_main(el_prod, el_price, cost_fixed, cost_marg)
#profit = model_main(el_prod, el_price, cost_fixed, cost_marg)

#(profit, ratio) = model_main(inputs_vector[0], inputs_vector[1],inputs_vector[2],inputs_vector[3])
answer = model_main(inputs_vector[0], inputs_vector[1],inputs_vector[2],inputs_vector[3])

profit = answer[0]
ratio = answer[1]

print answer
#print profit
#print ratio

def iterate_linear(start_value, target, error_limit, limit_iterations):
	"""This is an outer script, to iterate the values that you need."""
	
	#inputs_vector = [el_prod, el_price, cost_fixed, cost_marg] 	
	inputs_vector = [start_value, el_price, cost_fixed, cost_marg]
		
	ii = 0
	error = error_limit + 1 # to get while loop started
	
	answer = model_main(inputs_vector[0], inputs_vector[1],inputs_vector[2],inputs_vector[3])
	
	ini = range(limit_iterations + 1)
	ini[0] = float(answer[0])
	input = range(limit_iterations + 1)
	input[0] = float(start_value)
	
	print "ini = %d" % ini[0]
	
	inputs_vector[0] = inputs_vector[0] * 2 # (random() + 0.5) 
		
	
	for ii in range(1, limit_iterations):
					
		answer = model_main(inputs_vector[0], inputs_vector[1],inputs_vector[2],inputs_vector[3])
		# ini.append() = answer[0]
		ini[ii] = answer[0]
		input[ii] = inputs_vector[0]	
		
		change_answer = float(ini[ii] - ini[ii - 1])
		change_input = float(input[ii] - input[ii-1])  
		
		ratio =  change_answer / change_input
		
		#if ii > 1:
		#	trend_sol =  change_answer / (ini[ii-1] - ini[ii - 2])
		#else:
		#	trend_sol = 1 # assume linear trend at first
			
		# new_value = trend / 0.9 * target / inputs_vector[0]
		
		new_value = ((target - ini[ii]) / ratio + input[ii] )
		
	
		inputs_vector = [new_value, el_price, cost_fixed, cost_marg]
		
		print answer, inputs_vector[0], ii,  "ratio is ", ratio #, trend_sol
		
		if abs(answer[0] - target) < error_limit: # terminates the loop
			break
	
	return answer[0], inputs_vector[0], ii, ratio #, trend_sol

answer_2 = iterate_linear(el_prod, 5000000, 0.1, 100)
print answer_2