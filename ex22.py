# other 

# echo "text " > test.txt creates a test.txt file with text written in it
# to move with cd commands to a folder with spaces between, use "" 
# example cd "tanel joon"


# ex22 - recap

# from 1 - printing
print "Stuff" # or
print 'stuff'

# and add the next line in the beginning of code to have non-ASCII characters
# -*- coding: utf-8 -*-

# from 2 - more printing
# same printing

# from 3 - printing numbers, some calculaitons, logic checks
print "hens", 24 + 30 / 6
print "Is it greater pr equal?", 5 >= -2


print "7/2", 7/2
print "7.0/2.0", 7.0/2.0 # accuracy of solution, otherwise it rounds down

# % sign 
print 100%3 # % gives the remaining ie 1 in this case
print 3 + 1 + 1 - 5 + 4 % 2 - 1 / 4 + 6.00

# from 4 - printing and calcualtiong with arguments

cars = 100
drivers = 30
cars_to_drivers = cars / drivers
print "there are cars to drivers", cars_to_drivers, "in this city"

# from 5 - formats for printing
dude = "what"
print "dude %s dude %s" % (dude, dude)
print "dude %s"	% dude

# %s - string %r - for checking %d - for numbers decimal
number = 9000
print "it's over %d" % number

number_2 = 1

print "it's over %d" % (number + number_2)

# from 6 - assigning string values

x = "there are %d types of people." % 10
yes = "jah"
no = "ei"
y = "yes - %s no - %s" % (yes, no)
print x + y # print 2 strings

# from 7
print "." * 10 # many times print
print "what %s" % 'up'


# from 8 - using formatters

formatter = "%s %s %d %r"

print formatter % ('ship', "coast", 10+1.2, "bugger"+" your're")

# mult lines
print formatter % ("one",
"two",
10,
"more")

# from 9 - new line \n

print "Thursday \n Friday \n Saturday"

# or 

months = "Jan\nFeb\nMar\nApr\nMay\nJun\nJul\nAug"

print "%s" % months

# and double quotes
print """ yabba
dabba 
dooo
"""

# from 10 - different uses for the \ character
# really a lot of memorization
# just check 10 & 10_1 & 10_2 & 10_3


# from 11 - using raw_input(), and more printing
print "give value to ax"
ax = float(raw_input())
print "ax = %d" % ax

# quit() quits python, q quits long powershell lines

# from 12 - more printing and raw_input usage
age = raw_input("How old are you? ")

print "You are %r years old." % (age)

# from 13 - import arguments from sys

# from sys import argv - imoprt arguments
# script, first, second, third = argv - give names to the arguments or smth
# then use afterwards as you wish

# from 14 - asking import of parameters, and raw_input
# also describes that %s is for strings and %r is for 

# from 15 - open and read files, also close files

txt = open("test.txt")
print txt.read()
txt.close()

# from 16 - file opening reading etc operations

# close 
# read
# truncate - empties the file
# write - write into file

# use followingly
target = open("test.txt", 'w')
target.truncate()
target.write("first \n second \n etc")
target.close

#quit() - to quit the script, problem is this will also quit python
#print "XXX"

# from 17

# imort a function exists
from os.path import exists # this imports a function exists
in_file = open("text.txt")
indata = in_file.read()

to_file = "test.txt"
print "Does the output file exist? %r" % exists(to_file)

out_file = open(to_file, 'w')
out_file.write(indata)

out_file.close()
in_file.close()

# from 18 - making functions
def print_two(*args):
	arg1, arg2 = args
	print "arg1: %r, arg2: %r" % (arg1, arg2)

# same thing is 
def print_two_2(arg1, arg2):
	print "arg1 : %r, arg2: %r" % (arg1, arg2)

# usning the funciton
print_two("Kappa", "zappa")
print_two_2(3, 4)	

# from 19 - more functions
# passing argumets to functions

def cheese_and_crackers(cheese_count, crackers_count):
	print "You have %d cheeses!" % cheese_count
	print "You have %d crackers!" % crackers_count

cheese_and_crackers(20, 10)

cheese_and_crackers(15-2, 20*2)
temp1 = 20
temp2 = 30

cheese_and_crackers(temp1/temp2, temp2*8)

# from 20 - read and print files and arguments
def print_all(f):
	print f.read()

def rewind(f):
	f.seek(0)

def print_a_line(line_count, f):
	print line_count, f.readline()

current_file = open("ex15_sample.txt")
print_all(current_file)
rewind(current_file)
print_a_line(1, current_file)

# from 21 - even more functions
import math
#math.pi

def area_circle(diameter):
	# area in meters
	A = math.pi * diameter ** 2 / 4
	return A
	print "Area of the circle is %d " % A
	
temp = area_circle(5)
print "area is %d" % temp





