# ex19

# define a function with two variables
def cheese_and_crackers(cheese_count, boxes_of_crackers):
	# prints cheese_count
	print "You have %d cheeses!" % cheese_count
	# prints boxes_of_crackers
	print "You have%d boxes of crackers!" % boxes_of_crackers
	print "Man that's enough for a party!"
	print "Get a blanket.\n"

# use function with inputs
print "We can just give the function numbers directly:"
cheese_and_crackers(20, 30)

# use the function with other inputs
print "Or, we can use variables from our script:"
amount_of_cheese = 10
amount_of_crackers = 50

cheese_and_crackers(amount_of_cheese, amount_of_crackers)

# use function again
print "We can even do math inside too:"
cheese_and_crackers(10+20, 5+6)

# use it again
print "And we can combine the two, variables and math:"
cheese_and_crackers(amount_of_cheese + 10, amount_of_crackers+1000)

def print_twice(input_string):
	print "word twice is %s %s " % (input_string, input_string)
	
input_string = "work"

print_twice(input_string)
	
print_twice("work")

def square_num(inp_num):
	integer_num = int(inp_num)
	answer = integer_num**2
	print "square is %d" % answer

square_num(2)

number = 20

square_num(number)

print "give number to square :"
number = raw_input()

square_num(number)