# excercise 3 
print "I will now count my chickens:"
print "Hens", 25+30/6
print "Roosters", 100-25*3%4
print "Now I will count the eggs:"

# 3+2+1 -5 =1-5
#
print 3+2+1-5+4%2-1/4+6.00

print "Is it true that 3+2 < 5 - 7?"
print 3+2 < 5-7

print "What is 3 + 2?", 3 + 2
print "What is 5 - 7?", 5-7
print "Oh, that's why it's False."

print "How about some more."

print "Is it greater?", 5>-2
print "Is it greater or equal?", 5>=-2
print "Is it less or equal?", 5 <= -2

print "Waht does the % do?", 100%3
# the % gives the remaining of a division
# for example 100/33 remaining is 1, then 100%33 is 1 
print 4%2

# accuracy, division rounds down
print "7/2", 7/2
# if accuracy is added then the answer is more accurate as well
print "7.0/2.0", 7.0/2.0